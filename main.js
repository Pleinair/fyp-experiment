/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var fs = require('fs');
var http = require('http');
var url = require('url');
var util = require("util");
var express = require('express'); 
var cors = require('cors');
var path = require('path');
cors({credentials: true, origin: true});

var app = express();
app.use(cors());
app.use('/js', express.static(__dirname + '/js'));
app.use('/dist', express.static(__dirname + '/../dist'));
app.use('/css', express.static(__dirname + '/css'));
app.use('/partials', express.static(__dirname + '/partials'));
app.use('/controller', express.static(__dirname + '/views/controller'));
app.use('/router', express.static(__dirname + '/views/router'));
app.use('/view', express.static(__dirname + '/views'));

app.get('/content', function(req, res){
   res.header(contentType = 'application/json');
   
   var q = url.parse(req.url, true).topic;
   
   var content = {
       title: '',
       a1: [],
       a2: [],
       b1: [],
       b2: []
   };
   
   if(q == 'god'){
       content.title = 'God argument (Argument from design)';
       content.a1 = ['“In crossing a heath, suppose I pitched my foot against a stone, and were asked how the stone came to be there; I might possibly answer, that, for anything I knew to the contrary, it had lain there forever: nor would it perhaps be very easy to show the absurdity of this answer. But suppose I had found a watch upon the ground, and it should be inquired how the watch happened to be in that place; I should hardly think of the answer I had before given, that for anything I knew, the watch might have always been there. ... There must have existed, at some time, and at some place or other, an artificer or artificers, who formed [the watch] for the purpose which we find it actually to answer; who comprehended its construction, and designed its use. ... Every indication of contrivance, every manifestation of design, which existed in the watch, exists in the works of nature; with the difference, on the side of nature, of being greater or more, and that in a degree which exceeds all computation.” – William Paley, Natural Theology (1802)'];
       content.a2 = ['If you find a watch on a beach you would intuit that there was a watchmaker behind the watch.',
                    'This is because the watch is too complex to just come into existence on its own.',
                    'Therefore it is logical to think a complex being or a complex existence to have a creator.'];
       content.b1 = ['“But of course any God capable of intelligently designing something as complex as DNA/protein replicating machine must have been at least as complex and organized as the machine itself. Far more so if we suppose him additionally capable of such advanced functions as listening to prayers and forgiving sins. To explain the origin of the DNA/protein machine by invoking a supernatural Designer is to explain precisely nothing, for it leaves unexplained the origin of the Designer. You have to say something like "God was always there", and if you allow yourself that kind of lazy way out, you might as well just say "DNA was always there", or "Life was always there", and be done with it.” – Richard Dawkins, The Blind Watchmaker (1986)'];
       content.b2 = ['To argue that a creator exists for anything that is complex must necessarily mean the creator is also complex.',
                    'Given that the creator would then also have the ability to not only create but answer prayers and forgive sins.',
                    'This “explanation” explains nothing but provides more question as to the origin or nature of this infinitely complex creator.',
                    'If the creator can ‘always be there’ then why not just say the DNA was always there or life always there instead.'];
   }
   else if(q == 'sandwich'){
       content.title = 'Is hot dog a sandwich (Argument from definition)';
       content.a1 = ['“I’ve noticed this question coming up again and again. No. I don’t think it’s a sandwich. I don’t think a hamburger is a sandwich either. The fact that it’s in between bread–the bread is a delivery system, a ballistic delivery system. It is not a classic sandwich, in my view. I mean, if you were to talk into any vendor of fine hot dogs, and ask for a hot dog sandwich, they would probably report you to the FBI. As they should.” – Anthony Bourdain, Reddit AMA (2017)'];
       content.a2 = ['A sandwich is not colloquially defined or known as two breads with something between them',
                    'The bread in sandwich is simply the delivery system and not the manifestation of a sandwich'];
       content.b1 = ['“You can’t fight city hall, or the dictionary. Merriam Webster’s dictionary defines a sandwich as “two or more slices of bread or a split roll having a filling in between.” And while describing a wiener as “filling” might seem a bit awkward, it is the generic nature of the term “filling” that allows for such wondrous variety in sandwiches. “Filling” means freedom – the freedom to make bread a most versatile canvas – for peanut butter, egg salad, and even hot dogs.” - Chaya Benyamin, KnowYourMeme (2017)',
                    '“1a : two or more slices of bread or a split roll having a filling in between b : one slice of bread covered with food 2 : something resembling a sandwich especially : composite structural material consisting of layers often of high-strength facings bonded to a low strength central core” – Merriam Webster’s dictionary'];
       content.b2 = ['A dictionary is seen as an authority for word definitions, and the dictionary defines sandwiches as “two or more slices of bread or a split roll having a filling in between.”',
                    'A concept fitting awkwardly with a definition of the concept does not make the definition false.'];
   }
   else if(q == 'lgbt'){
       content.title = 'Should LGBT be tolerated? (Argument from repercussions)';
       content.a1 = ['Something something stop being gay'];
       content.a2 = ['No one will reproduce'];
       content.b1 = ['Something something people should be able to choose'];
       content.b2 = ["Doesn't hurt anyone else"];
   }
   else{
       content.title = 'error';
       content.a1 = ['inform one of us that something went wrong'];
       content.a2 = ['you were not supposed to see this page'];
       content.b1 = ['should probably stop the thing now'];
       content.b2 = ['hmm, this is extremely unfortunate'];
   }
   
   res.json(content);
});

app.get('/form', function(req, res){
    res.end('form saved');
});

app.route('/web/*').get((req, res) => {
    res.sendFile(path.resolve(__dirname + '/views/index.html'));
});

app.listen(1010, function(){
    console.log("Hey it's allowed here");
});

//http.createServer(function (req, res) {
//    var q = url.parse(req.url, true);
//    var filename = '.' + q.pathname;
//
////    fs.readFile(filename, function(err, data){
////        if (err) {
////          res.writeHead(404, {'Content-Type': 'text/html'});
////          return res.end("404 Not Found");
////        }
////        console.log(filename+" this is the file btw");
////        res.writeHead(200, {'Content-Type' : 'text/html'});
////        res.write(data); //write a response to the client
////        return res.end();
////    }); //end the response
//}).listen(1010); //the server object listens on port 8080

