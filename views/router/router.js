var app = angular.module("myApp", ["ngRoute", "controllers"]);
app.config(function($routeProvider){
    $routeProvider
        .when("/1", {
            templateUrl: "/view/first.html",
            controller: "hilmanCtrl"
        })
        .when("/2", {
            templateUrl: "/view/title.html",
            controller: "nizrulCtrl"
        })
        .when("/3", {
            templateUrl: "/view/a.html",
            controller: "nizrulCtrl"
        })
        .when("/4", {
            templateUrl: "/view/a.html",
            controller: "nizrulCtrl"
        })
        .when("/5", {
            templateUrl: "/view/b.html",
            controller: "nizrulCtrl"
        })
        .when("/6", {
            templateUrl: "/view/b.html",
            controller: "nizrulCtrl"
        })
        .when("/7", {
            templateUrl: "/view/choice.html",
            controller: "nizrulCtrl"
        })
        .when("/8", {
            templateUrl: "/view/a.html",
            controller: "nizrulCtrl"
        })
        .when("/9", {
            templateUrl: "/view/a.html",
            controller: "nizrulCtrl"
        })
        .when("/10", {
            templateUrl: "/view/b.html",
            controller: "nizrulCtrl"
        })
        .when("/11", {
            templateUrl: "/view/b.html",
            controller: "nizrulCtrl"
        })
        .when("/12", {
            templateUrl: "/view/choice.html",
            controller: "nizrulCtrl"
        })
        .when("/13", {
            templateUrl: "/view/a.html",
            controller: "nizrulCtrl"
        })
        .when("/14", {
            templateUrl: "/view/a.html",
            controller: "nizrulCtrl"
        })
        .when("/15", {
            templateUrl: "/view/b.html",
            controller: "nizrulCtrl"
        })
        .when("/16", {
            templateUrl: "/view/b.html",
            controller: "nizrulCtrl"
        })
        .when("/17", {
            templateUrl: "/view/choice.html",
            controller: "nizrulCtrl"
        })
        .when("/18", {
            templateUrl: "/view/last.html",
            controller: "hilmanCtrl"
        })
        .when("/home", {
            template: "<h1>HOME</h1>"
        })
        .otherwise({
            redirectTo: "/home"
        });
    });

